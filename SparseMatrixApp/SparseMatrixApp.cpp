// SparseMatrixApp.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include "ViewController.h"
#include "ApplicationController.h"

int main()
{
	ApplicationController application_controller;
	application_controller.run();
    return 0;
}
