#pragma once

enum class ResultCode
{
	SUCCESS,
	ILLEGAL_ARGUMENT,
	FAILURE
};