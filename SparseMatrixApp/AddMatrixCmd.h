﻿#pragma once
#include "ICommand.h"
#include <vector>

class AddMatrixCmd : public ICommand
{
public:
	AddMatrixCmd(int dimensions, std::vector<int> sizes, int default_val, std::string name = "");
	void execute(Result** result, ModelController** model) override;

	static const int MIN_ARGUMENTS = 4;
	static const int DIMENSIONS_POS = 1;

private:
	int _dimensions;
	std::vector<int> _sizes;
	int _default_val;
	std::string _name;

};
