﻿#pragma once
#include "ResultCode.h"
#include <string>

class Result
{
public:
	Result(ResultCode result_code);
	Result(ResultCode result_code, std::string message);
	ResultCode getResultCode();
	std::string getMessage();
	void setMessage(std::string message) { _message = message; }

private:
	ResultCode _result_code;
	std::string _message;
};
