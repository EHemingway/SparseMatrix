﻿#include "stdafx.h"
#include "Result.h"

static const std::string EMPTY_MSG = "";

Result::Result(ResultCode result_code) :
	_result_code(result_code),
	_message(EMPTY_MSG)
	{ }

Result::Result(ResultCode result_code, std::string message):
	_result_code(result_code),
	_message(message)
	{}

ResultCode Result::getResultCode()
{
	return _result_code;
}

std::string Result::getMessage()
{
	return _message;
}
