﻿#pragma once
#include "ICommand.h"

class RenameCmd: public ICommand
{
public:
	RenameCmd(int position, std::string new_name);
	void execute(Result** result, ModelController** model) override;

	const static int MIN_ARGS = 3;
	const static int POSITION_ARG_POS = 1;
	const static int NEW_NAME_POS = 2;
	
private:
	int _position;
	std::string _new_name;
};
