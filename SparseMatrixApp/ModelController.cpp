﻿#include "stdafx.h"
#include "ModelController.h"
#include "ResultCode.h"
#include "Result.h"
#include <sstream>

const static std::string DESTROY = "Destroy: ";

ModelController::ModelController():
	_sparse_matrix_array_len(0)
{
	_sparse_matrix_array = new SparseMatrix*[0];
	
}

void ModelController::addNewMatrix(SparseMatrix* matrix)
{
	SparseMatrix** new_matrix_arr = new SparseMatrix*[_sparse_matrix_array_len + 1];
	for (int i = 0; i < _sparse_matrix_array_len; i++)
	{
		new_matrix_arr[i] = _sparse_matrix_array[i];
	}
	new_matrix_arr[_sparse_matrix_array_len] = matrix;
	_sparse_matrix_array_len++;
	delete[] _sparse_matrix_array;
	_sparse_matrix_array = new_matrix_arr;
}

SparseMatrix* ModelController::getSparseMatrix(int offset)
{
	if (offset > _sparse_matrix_array_len - 1 || offset < 0)
	{
		return nullptr;
	}
	return _sparse_matrix_array[offset];
}

void ModelController::removeMatrixAt(int position, Result** result)
{
	if (position < 0 || position >= _sparse_matrix_array_len)
	{
		*result = new Result(ResultCode::ILLEGAL_ARGUMENT);
		return;
	}
	SparseMatrix** new_matrix_arr = new SparseMatrix*[_sparse_matrix_array_len - 1];
	for (int i = 0; i < _sparse_matrix_array_len; i++)
	{
		if (i < position)
		{
			new_matrix_arr[i] = _sparse_matrix_array[i];
		}
		if(i > position)
		{
			new_matrix_arr[i - 1] = _sparse_matrix_array[i];
		}
	}
	std::ostringstream stream;
	stream << DESTROY << _sparse_matrix_array[position]->getName();

	*result = new Result(ResultCode::SUCCESS, stream.str());
	_sparse_matrix_array_len--;
	delete[] _sparse_matrix_array;
	_sparse_matrix_array = new_matrix_arr;
}
