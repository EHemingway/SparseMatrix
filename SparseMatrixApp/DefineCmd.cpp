﻿#include "stdafx.h"
#include "DefineCmd.h"

static const std::string NO_MATRIX = "There is no matrix on that posisition";

DefineCmd::DefineCmd(int matrix_position, std::vector<int> coordinates, int value) :
	_matrix_position(matrix_position),
	_coordinates(coordinates),
	_value(value)
	{ }

void DefineCmd::execute(Result** result, ModelController** model)
{
	SparseMatrix* matrix = (*model)->getSparseMatrix(_matrix_position);
	if (matrix != nullptr)
	{
		matrix->addCell(_coordinates, _value, result);
	}
	else
	{
		*result = new Result(ResultCode::ILLEGAL_ARGUMENT, NO_MATRIX);
	}
}
