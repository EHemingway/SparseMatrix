﻿#pragma once
#include "ICommand.h"

class CloneCmd: public ICommand
{
public:
	CloneCmd(int position): _position(position) { }
	void execute(Result** result, ModelController** model) override;

	const static int MIN_ARGS = 2;
	const static int POSITION_ARG_POS = 1;
	
private:
	int _position;
};
