﻿#pragma once
#include "ICommand.h"

class ListCmd: public ICommand
{
public:
	void execute(Result** result, ModelController** model) override;
	
};
