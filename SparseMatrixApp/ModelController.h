﻿#pragma once
#include "SparseMatrix.h"

class Result;

class ModelController
{
public:
	ModelController();
	void addNewMatrix(SparseMatrix* matrix);
	int getSparseMatrixArrLen() { return _sparse_matrix_array_len;  }
	SparseMatrix* getSparseMatrix(int offset);
	void removeMatrixAt(int position, Result** result);

private:
	SparseMatrix** _sparse_matrix_array;
	int _sparse_matrix_array_len;
};
