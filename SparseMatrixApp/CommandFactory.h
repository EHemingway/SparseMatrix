﻿#pragma once
#include "ICommand.h"
#include <vector>

class CommandFactory
{
public:
	static ICommand* getCommand(std::vector<std::string> input, Result** result);

private:

};

namespace CommandNames
{
	const std::string ADD_MATRIX = "addmat";
	const std::string LIST = "list";
	const std::string DELETE = "del";
	const std::string DELETE_ALL = "delall";
	const std::string DEFINE = "def";
	const std::string PRINT = "print";
	const std::string CLONE = "clone";
	const std::string RENAME = "rename";
}