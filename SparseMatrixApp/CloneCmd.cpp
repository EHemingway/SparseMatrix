﻿#include "stdafx.h"
#include "CloneCmd.h"

const static std::string CREATE_MSG = "Create: ";

void CloneCmd::execute(Result** result, ModelController** model)
{
	SparseMatrix* matrix_to_clone = (*model)->getSparseMatrix(_position);
	SparseMatrix* matrix = new SparseMatrix(matrix_to_clone);
	(*model)->addNewMatrix(matrix);
	*result = new Result(ResultCode::SUCCESS, CREATE_MSG + matrix->getName());
}
