﻿#pragma once
#include <string>
#include <vector>
#include "SparseMatrixCell.h"
#include "Result.h"

class SparseMatrix
{
public:
	SparseMatrix();
	SparseMatrix(int dimensions, int default_val);
	SparseMatrix(int dimensions, int default_val, std::string name);
	SparseMatrix(SparseMatrix* sparse_matrix);
	std::string getName() { return _name; }
	int getDimensions() { return _dimensions; }
	int getSize(int dimension);
	void setSizes(std::vector<int> sizes, Result** result);
	void addCell(std::vector<int> coordinates, int value, Result** result);
	std::string getInfo();
	void rename(std::string new_name) { _name = new_name; }
	~SparseMatrix();

private:
	int* _sizes;
	SparseMatrixCell** _cells;
	int _cells_len;
	std::string _name;
	int _dimensions;
	int _default_val;

	void removeCell(int position);
};
