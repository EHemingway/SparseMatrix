﻿#pragma once
#include "ICommand.h"

class DeleteAllCmd: public ICommand
{
public:
	DeleteAllCmd() { };
	void execute(Result** result, ModelController** model) override;
};
