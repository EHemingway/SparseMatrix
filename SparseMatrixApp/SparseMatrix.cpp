﻿#include "stdafx.h"
#include "SparseMatrix.h"
#include <sstream>

static const std::string DEFAULT_NAME = "SparseMatrixDef";
static const int DEFAULT_DIMENSIONS = 1;
static const int DEFAULT_DEFAULT_VAL = 0;
static const std::string NOT_ENOUGH_SIZES = "The number of sizes doesnt match the dimensions of the matrix";
static const std::string NOT_ENOUGH_COORDINATES = "The number of coordinates doesnt match the dimensions of the matrix";
static const std::string TOO_LARGE_COORDINATES = "The coordinate exeeds matrix size";
static const std::string SIZE = "size : ";
static const std::string LEFT_BRAC = "[";
static const std::string RIGHT_BRAC = "]";
static const std::string VALUES = " values: ";
static const std::string COLON = ":";
static const std::string SEMI_COLON = "; ";
static const std::string COMMA = ",";
static const std::string COPY = "_copy";


SparseMatrix::SparseMatrix(): SparseMatrix(DEFAULT_DIMENSIONS, DEFAULT_DEFAULT_VAL, DEFAULT_NAME)
{
}

SparseMatrix::SparseMatrix(int dimensions, int default_val): SparseMatrix(dimensions, default_val, DEFAULT_NAME)
{
}

SparseMatrix::SparseMatrix(int dimensions, int default_val, std::string name) :
	_name(name),
	_dimensions(dimensions),
	_default_val(default_val)
{
	_sizes = new int[dimensions];
	_cells = new SparseMatrixCell*[0];
	_cells_len = 0;
}

SparseMatrix::SparseMatrix(SparseMatrix* sparse_matrix): 
	SparseMatrix(
		sparse_matrix->getDimensions(),
		sparse_matrix->_default_val,
		sparse_matrix->getName() + COPY)
{
	for(int i = 0; i < _dimensions; i++)
	{
		_sizes[i] = sparse_matrix->_sizes[i];
	}
	delete[] _cells;
	_cells_len = sparse_matrix->_cells_len;
	_cells = new SparseMatrixCell*[_cells_len];
	for (int i = 0; i < _cells_len; i++)
	{
		_cells[i] = new SparseMatrixCell(*sparse_matrix->_cells[i]);
	}
}

int SparseMatrix::getSize(int dimension)
{
	if (dimension > _dimensions || dimension < 1)
	{
		return -1;
	}
	return _sizes[dimension - 1];
}

void SparseMatrix::setSizes(std::vector<int> sizes, Result** result)
{
	if (sizes.size() != _dimensions)
	{
		*result = new Result(ResultCode::FAILURE, NOT_ENOUGH_SIZES);
	}
	else
	{
		for (int i = 0; i < _dimensions; i++)
		{
			_sizes[i] = sizes[i];
		}
		*result = new Result(ResultCode::SUCCESS);
	}
}

void SparseMatrix::addCell(std::vector<int> coordinates, int value, Result** result)
{
	if (coordinates.size() != _dimensions)
	{
		*result = new Result(ResultCode::FAILURE, NOT_ENOUGH_COORDINATES);
	}
	else
	{
		for (int i = 0; i < coordinates.size(); i++)
		{
			if (coordinates[i] >= _sizes[i] || coordinates[i] < 0)
			{
				*result = new Result(ResultCode::ILLEGAL_ARGUMENT, TOO_LARGE_COORDINATES);
				return;
			}
		}
		if (value != _default_val)
		{
			SparseMatrixCell** new_cells = new SparseMatrixCell*[_cells_len + 1];
			for (int i = 0; i < _cells_len; i++)
			{
				new_cells[i] = _cells[i];
			}
			new_cells[_cells_len] = new SparseMatrixCell(coordinates, value);
			_cells_len++;
			delete[] _cells;
			_cells = new_cells;
			*result = new Result(ResultCode::SUCCESS);
		}
		else
		{
			bool is_same = false;
			int same = 0;
			for (int i = 0; i < _cells_len && !is_same; i++, same++)
			{
				is_same = true;
				for (int j = 0; j < _dimensions; j++)
				{
					if (_cells[i]->_coordinates[j] != coordinates[j])
					{
						is_same = false;
					}
				}
			}
			if (is_same)
			{
				removeCell(same);
			}
			*result = new Result(ResultCode::SUCCESS);
		}
	}
}

std::string SparseMatrix::getInfo()
{
	std::ostringstream info;
	info << SIZE << LEFT_BRAC;
	for (int i = 0; i < _dimensions; i++)
	{
		info << _sizes[i];
		if (_dimensions - i > 1)
		{
			info << COMMA;
		}
	}
	info << RIGHT_BRAC << VALUES;
	for (int i = 0; i < _cells_len; i++)
	{
		info << LEFT_BRAC;
		for (int j = 0; j < _dimensions; j++)
		{
			info << _cells[i]->_coordinates[j];
			if (_dimensions - j > 1)
			{
				info << COMMA;
			}
		}
		info << RIGHT_BRAC << COLON << _cells[i]->_value << SEMI_COLON;
	}
	return info.str();
}

SparseMatrix::~SparseMatrix()
{
	delete[] _sizes;
	for (int i = 0; i < _cells_len; i++)
	{
		delete _cells[i];
	}
	delete[] _cells;
	_sizes = nullptr;
	_cells = nullptr;
}

void SparseMatrix::removeCell(int position)
{
	SparseMatrixCell** new_cells = new SparseMatrixCell*[_cells_len - 1];
	for (int i = 0; i < _cells_len; i++)
	{
		if (i < position)
		{
			new_cells[i] = _cells[i];
		}
		if (i > position)
		{
			new_cells[i - 1] = _cells[i];
		}
	}
	delete _cells[position];
	delete[] _cells;
	_cells_len--;
	_cells = new_cells;
}
