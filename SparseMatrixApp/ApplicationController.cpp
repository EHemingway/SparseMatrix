﻿#include "stdafx.h"
#include "ApplicationController.h"
#include <string>
#include <iostream>
#include "AddMatrixCmd.h"

ApplicationController::ApplicationController()
{
	_view = new ViewController();
	_model = new ModelController();
}

void ApplicationController::run()
{
	while (true)
	{
		ICommand* command = _view->getCommand();
		if (command != nullptr)
		{
			Result* command_result = nullptr;
			command->execute(&command_result, &_model);
			if (command_result->getResultCode() == ResultCode::SUCCESS && command_result->getMessage() != "")
			{
				_view->printInfoMessage(command_result->getMessage());
			}
			else if (command_result->getMessage() != "")
			{
				_view->printErrorMessage(command_result->getMessage());
			}
		}
	}
}
