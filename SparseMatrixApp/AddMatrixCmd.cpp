﻿#include "stdafx.h"
#include "AddMatrixCmd.h"
#include <sstream>

const static std::string CREATE_MSG = "Create: ";

AddMatrixCmd::AddMatrixCmd(int dimensions, std::vector<int> sizes, int default_val, std::string name) :
	_dimensions(dimensions),
	_sizes(sizes),
	_default_val(default_val),
	_name(name)
	{ }

void AddMatrixCmd::execute(Result** result, ModelController** model)
{
	SparseMatrix* matrix;
	if (_name == "")
	{
		matrix = new SparseMatrix(_dimensions, _default_val);
	}
	else
	{
		matrix = new SparseMatrix(_dimensions, _default_val, _name);
	}
	matrix->setSizes(_sizes, result);
	if ((*result)->getResultCode() == ResultCode::SUCCESS)
	{
		(*model)->addNewMatrix(matrix);
		std::ostringstream msg_stream;
		msg_stream << CREATE_MSG << matrix->getName();
		(*result)->setMessage(msg_stream.str());
	}
}

