﻿#include "stdafx.h"
#include "DeleteCmd.h"
#include <sstream>

const static std::string WRONG_ARG = "Wrong offset";

void DeleteCmd::execute(Result** result, ModelController** model)
{
	(*model)->removeMatrixAt(_position, result);
	if ((*result)->getResultCode() != ResultCode::SUCCESS)
	{
		(*result)->setMessage(WRONG_ARG);
	}
}
