﻿#include "stdafx.h"
#include "PrintCmd.h"

static const std::string NO_MATRIX = "There's no matrix on that offset";

void PrintCmd::execute(Result** result, ModelController** model)
{
	SparseMatrix* matrix = (*model)->getSparseMatrix(_position);
	if (matrix == nullptr)
	{
		*result = new Result(ResultCode::ILLEGAL_ARGUMENT, NO_MATRIX);
	}
	else
	{
		*result = new Result(ResultCode::SUCCESS, matrix->getInfo());
	}
}
