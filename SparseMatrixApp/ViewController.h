﻿#pragma once
#include "ICommand.h"
#include <vector>
#include <string>

class ViewController
{
public:
	ICommand* getCommand();
	void printErrorMessage(std::string message);
	void printInfoMessage(std::string message);

private:
	std::vector<std::string> parseInputLine(std::string input_line);
	void printInputErrorMessage(std::string message);
};
