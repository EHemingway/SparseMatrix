﻿#pragma once
#include <vector>
#include "ICommand.h"

class DefineCmd: public ICommand
{
public:
	DefineCmd(int matrix_position, std::vector<int> coordinates, int value);
	void execute(Result** result, ModelController** model) override;

	const static int MIN_ARGS = 4;
	const static int MATRIX_POS_POS = 1;
	const static int COORD_START = 2;
	const static int VALUE_OFF = 1;

private:
	int _matrix_position;
	std::vector<int> _coordinates;
	int _value;
};
