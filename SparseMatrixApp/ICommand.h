#pragma once
#include "Result.h"
#include "ModelController.h"

class ICommand
{
public:
	ICommand() { };
	virtual void execute(Result** result, ModelController** model) = 0;
};
