﻿#include "stdafx.h"
#include "SparseMatrixCell.h"


SparseMatrixCell::SparseMatrixCell(std::vector<int> coordinates, int value):
	_value(value)
{
	_coordinates = new int[coordinates.size()];
	_dimensions = coordinates.size();
	for (int i = 0; i < _dimensions; i++)
	{
		_coordinates[i] = coordinates[i];
	}
}
