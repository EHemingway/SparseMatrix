﻿#include "stdafx.h"
#include "RenameCmd.h"

RenameCmd::RenameCmd(int position, std::string new_name): 
	_position(position),
	_new_name(new_name)
	{ }

void RenameCmd::execute(Result** result, ModelController** model)
{
	(*model)->getSparseMatrix(_position)->rename(_new_name);
	*result = new Result(ResultCode::SUCCESS);
}
