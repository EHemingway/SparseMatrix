﻿#include "stdafx.h"
#include "CommandFactory.h"
#include <vector>
#include "AddMatrixCmd.h"
#include "ListCmd.h"
#include "DeleteCmd.h"
#include "DeleteAllCmd.h"
#include "DefineCmd.h"
#include "PrintCmd.h"
#include "CloneCmd.h"
#include "RenameCmd.h"

static const std::string EMPTY_COMMAND_MSG = "The command cannot be empty";
static const std::string NOT_ENOUGH_ARGS = "Not enough arguments";
static const std::string NOT_INT_ARG = "Argument has to be int";
static const std::string UNKNOWN_COMMAND = "Unknown command";

ICommand* CommandFactory::getCommand(std::vector<std::string> input, Result** result)
{
	if (input.size() == 0)
	{
		*result = new Result(ResultCode::FAILURE, EMPTY_COMMAND_MSG);
		return nullptr;
	}
	std::string command_name = input[0];

	if (command_name == CommandNames::ADD_MATRIX)
	{
		if (input.size() < AddMatrixCmd::MIN_ARGUMENTS)
		{
			*result = new Result(ResultCode::FAILURE, NOT_ENOUGH_ARGS);
			return nullptr;
		}
		try
		{
			int dimensions = std::stoi(input[AddMatrixCmd::DIMENSIONS_POS]);
			if (input.size() < AddMatrixCmd::MIN_ARGUMENTS + dimensions - 1)
			{
				*result = new Result(ResultCode::FAILURE, NOT_ENOUGH_ARGS);
				return nullptr;
			}

			std::vector<int> sizes;
			for (int i = 1; i <= dimensions; i++)
			{
				sizes.push_back(std::stoi(input[i + AddMatrixCmd::DIMENSIONS_POS]));
			}
			int default_val_pos = dimensions + AddMatrixCmd::DIMENSIONS_POS + 1;
			int default_val = std::stoi(input[default_val_pos]);
			*result = new Result(ResultCode::SUCCESS);
			if (input.size() > default_val_pos + 1)
			{
				return new AddMatrixCmd(dimensions, sizes, default_val, input[default_val_pos + 1]);
			}
			
			return new AddMatrixCmd(dimensions, sizes, default_val);
		}
		catch (...)
		{
			*result = new Result(ResultCode::ILLEGAL_ARGUMENT, NOT_INT_ARG);
			return nullptr;
		}
	}

	if (command_name == CommandNames::LIST)
	{
		*result = new Result(ResultCode::SUCCESS);
		return new ListCmd();
	}
	
	if(command_name == CommandNames::DELETE)
	{
		if (input.size() < DeleteCmd::MIN_ARGS)
		{
			*result = new Result(ResultCode::FAILURE, NOT_ENOUGH_ARGS);
			return nullptr;
		}
		try
		{
			int position = std::stoi(input[DeleteCmd::POSITION_ARG_POS]);
			*result = new Result(ResultCode::SUCCESS);
			return new DeleteCmd(position);
		}
		catch (...)
		{
			*result = new Result(ResultCode::ILLEGAL_ARGUMENT, NOT_INT_ARG);
			return nullptr;
		}
	}

	if (command_name == CommandNames::DELETE_ALL)
	{
		*result = new Result(ResultCode::SUCCESS);
		return new DeleteAllCmd();
	}

	if (command_name == CommandNames::DEFINE)
	{
		if (input.size() < DefineCmd::MIN_ARGS)
		{
			*result = new Result(ResultCode::FAILURE, NOT_ENOUGH_ARGS);
			return nullptr;
		}
		try
		{
			int matrix_position = std::stoi(input[DefineCmd::MATRIX_POS_POS]);
			std::vector<int> cooridinates;
			for (int i = DefineCmd::COORD_START; i < input.size() - 1; i++)
			{
				cooridinates.push_back(std::stoi(input[i]));
			}
			int value = std::stoi(input[input.size() - DefineCmd::VALUE_OFF]);
			*result = new Result(ResultCode::SUCCESS);
			return new DefineCmd(matrix_position, cooridinates, value);
		}
		catch (...)
		{
			*result = new Result(ResultCode::ILLEGAL_ARGUMENT, NOT_INT_ARG);
			return nullptr;
		}
	}

	if (command_name == CommandNames::PRINT)
	{
		if (input.size() < PrintCmd::MIN_ARGS)
		{
			*result = new Result(ResultCode::FAILURE, NOT_ENOUGH_ARGS);
			return nullptr;
		}

		try
		{
			int position = std::stoi(input[PrintCmd::POSITION_ARG_POS]);
			*result = new Result(ResultCode::SUCCESS);
			return new PrintCmd(position);
		}
		catch (...)
		{
			*result = new Result(ResultCode::ILLEGAL_ARGUMENT, NOT_INT_ARG);
			return nullptr;
		}
	}

	if (command_name == CommandNames::CLONE)
	{
		if (input.size() < CloneCmd::MIN_ARGS)
		{
			*result = new Result(ResultCode::FAILURE, NOT_ENOUGH_ARGS);
			return nullptr;
		}
		try
		{
			int position = std::stoi(input[CloneCmd::POSITION_ARG_POS]);
			*result = new Result(ResultCode::SUCCESS);
			return new CloneCmd(position);
		}
		catch (...)
		{
			*result = new Result(ResultCode::ILLEGAL_ARGUMENT, NOT_INT_ARG);
			return nullptr;
		}
	}

	if (command_name == CommandNames::RENAME)
	{
		if(input.size() < RenameCmd::MIN_ARGS)
		{
			*result = new Result(ResultCode::FAILURE, NOT_ENOUGH_ARGS);
			return nullptr;
		}

		try
		{
			int position = std::stoi(input[RenameCmd::POSITION_ARG_POS]);
			std::string new_name = input[RenameCmd::NEW_NAME_POS];
			*result = new Result(ResultCode::SUCCESS);
			return new RenameCmd(position, new_name);
		}
		catch (...)
		{
			*result = new Result(ResultCode::ILLEGAL_ARGUMENT, NOT_INT_ARG);
			return nullptr;
		}
	}

	*result = new Result(ResultCode::FAILURE, UNKNOWN_COMMAND);
	return nullptr;
}

