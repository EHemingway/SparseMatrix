﻿#pragma once
#include <vector>

class SparseMatrixCell
{
public:
	friend class SparseMatrix;
	SparseMatrixCell(std::vector<int> coordinates, int value);

private:
	int* _coordinates;
	int _dimensions;
	int _value;
};
