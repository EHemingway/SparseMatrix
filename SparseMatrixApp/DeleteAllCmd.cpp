﻿#include "stdafx.h"
#include "DeleteAllCmd.h"
#include <sstream>

const static std::string WRONG_ARG = "Wrong offset";
const static std::string NEW_LINE = "\n";

void DeleteAllCmd::execute(Result** result, ModelController** model)
{
	std::ostringstream message;
	while((*model)->getSparseMatrixArrLen() != 0)
	{
		Result* partial_result;
		(*model)->removeMatrixAt(0, &partial_result);
		if (partial_result->getResultCode() != ResultCode::SUCCESS)
		{
			*result = new Result(partial_result->getResultCode(), WRONG_ARG);
		}
		else
		{
			message << partial_result->getMessage() + NEW_LINE;
		}
	}
	if (*result == nullptr)
	{
		*result = new Result(ResultCode::SUCCESS, message.str());
	}
}
