﻿#include "stdafx.h"
#include "ListCmd.h"
#include <sstream>

const static std::string MATRICES = " matrices: \n";
const static std::string LEFT_SQR_BRACE = "[";
const static std::string RIGHT_SQR_BRACE = "]";
const static std::string DASH = " - ";
const static std::string SIZE = " size ";
const static std::string COMMA = ", ";
const static std::string ENDL = "\n";

void ListCmd::execute(Result** result, ModelController** model)
{
	std::ostringstream string_stream;
	string_stream << (*model)->getSparseMatrixArrLen() << MATRICES;
	for (int i = 0; i < (*model)->getSparseMatrixArrLen(); i++)
	{
		string_stream << LEFT_SQR_BRACE << i << RIGHT_SQR_BRACE;
		string_stream << DASH;
		SparseMatrix* matrix = (*model)->getSparseMatrix(i);
		string_stream << matrix->getName();
		string_stream << SIZE << LEFT_SQR_BRACE;
		for (int j = 1; j <= matrix->getDimensions(); j++)
		{
			string_stream << matrix->getSize(j) << COMMA;
		}
		string_stream << RIGHT_SQR_BRACE << ENDL;
	}
	*result = new Result(ResultCode::SUCCESS, string_stream.str());
}
