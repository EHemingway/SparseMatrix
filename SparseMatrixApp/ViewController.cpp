﻿#include "stdafx.h"
#include "ViewController.h"
#include <iostream>
#include <vector>
#include <sstream>
#include "CommandFactory.h"

static const char COMMAND_DELIMITER = ' ';
static const std::string INPUT_ERROR_MSG_TEMPLATE = "There was an error parsing command: ";
static const std::string ERROR = "[ERROR]: ";
static const std::string INFO = "[INFO]: ";

ICommand* ViewController::getCommand()
{
	std::string input_line;
	std::getline(std::cin, input_line);
	std::vector<std::string> input_parsed = parseInputLine(input_line);
	Result* command_factory_result;
	ICommand* command = CommandFactory::getCommand(input_parsed, &command_factory_result);
	if (command_factory_result->getResultCode() != ResultCode::SUCCESS)
	{
		printInputErrorMessage(command_factory_result->getMessage());
		return nullptr;
	}
	return command;
}

void ViewController::printErrorMessage(std::string message)
{
	std::cout << ERROR << message << std::endl;
}

void ViewController::printInfoMessage(std::string message)
{
	std::cout << INFO << message << std::endl;
}


std::vector<std::string> ViewController::parseInputLine(std::string input_line)
{
	std::vector<std::string> tokens;
	std::istringstream stream(input_line);
	for (std::string token; std::getline(stream, token, COMMAND_DELIMITER);)
	{
		tokens.push_back(token);
	}
	return tokens;
}

void ViewController::printInputErrorMessage(std::string message)
{
	std::cout << INPUT_ERROR_MSG_TEMPLATE << message << std::endl;
}
