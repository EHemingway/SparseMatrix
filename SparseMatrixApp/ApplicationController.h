﻿#pragma once
#include "ViewController.h"
#include "SparseMatrix.h"
#include "ModelController.h"

class ApplicationController
{
public:
	ApplicationController();
	void run();

private:
	ViewController* _view;
	ModelController* _model;

};
